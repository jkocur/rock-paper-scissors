# Rock-Paper-Scissors

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Status](#status)


## General info
Simple web application, rock paper scissors game


## Technologies
* Javascript
* HTML + CSS


## Status
Project is:  _finished_


