let userScore = 0;
let computerScore = 0;
const userScore_span = document.getElementById("user-score");
const compScore_span = document.getElementById("comp-score");
const scoreBoard_div = document.querySelector(".score-board");
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissor_div = document.getElementById("s");


function getCompChoice() {
    const choices = ['r', 'p', 's'];
    const randomNum = (Math.floor(Math.random() * 3));
    return choices[randomNum];
}

function convertToWord(letter) {
    switch(letter) {
        case "r":
            return "Rock";
            break;
        case "s":
            return "Scissor";
            break;
        case "p":
            return "Paper";
            break;
    }
}

function win(userChoice, computerChoice) {
    userScore++;
    userScore_span.innerHTML = userScore;
    compScore_span.innerHTML = computerScore;
    const smallUserWord = "user".fontsize(3).sub();
    const smallCompWord = "comp".fontsize(3).sub();
    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord}  beats  ${convertToWord(computerChoice)}${smallCompWord}. You WIN! `;
    document.getElementById(userChoice).classList.add('green-glow');
    setTimeout(() => {document.getElementById(userChoice).classList.remove('green-glow')}, 300);

}



function lose(userChoice, computerChoice) {
    computerScore++;
    compScore_span.innerHTML = computerScore;
    userScore_span.innerHTML = userScore;
    const smallUserWord = "user".fontsize(3).sub();
    const smallCompWord = "comp".fontsize(3).sub();
    result_p.innerHTML = `${convertToWord(computerChoice)}${smallCompWord}  beats  ${convertToWord(userChoice)}${smallUserWord}. You Lost! `;
    document.getElementById(userChoice).classList.add('red-glow');
    setTimeout(() => {document.getElementById(userChoice).classList.remove('red-glow')}, 300);

}

function draw(userChoice, computerChoice) {
    const smallUserWord = "user".fontsize(3).sub();
    const smallCompWord = "comp".fontsize(3).sub();
    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} neutralize  ${convertToWord(computerChoice)}${smallCompWord} . It's draw! `;
    document.getElementById(userChoice).classList.add('grey-glow');
    setTimeout(() => {document.getElementById(userChoice).classList.remove('grey-glow')}, 300);

}

function game(userChoice) {
    const compChoice = getCompChoice();
    switch (userChoice + compChoice) {
        case "rp":
            lose(userChoice, compChoice);
            break;
        case "pr":
            win(userChoice, compChoice);
            break;
        case "sp":
            win(userChoice, compChoice);
            break;
        case "ps":
            lose(userChoice, compChoice);
            break;
        case "rs":
            win(userChoice, compChoice);
            break;
        case "sr":
            lose(userChoice, compChoice);
            break;
        case "ss":
            draw(userChoice, compChoice);
            break;
        case "rr":
            draw(userChoice, compChoice);
            break;
        case "pp":
            draw(userChoice, compChoice);
            break;

    }
}

game("c");
function main(){

    rock_div.addEventListener('click', function () {
        game('r');

    })

    paper_div.addEventListener('click', function () {
        game('p');

    })

    scissor_div.addEventListener('click', function () {
        game('s');

    })

}
main();